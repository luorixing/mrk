#include <ignition/math/Pose3.hh>
#include "gazebo/physics/physics.hh"
#include "gazebo/common/common.hh"
#include "gazebo/gazebo.hh"

namespace gazebo
{
class Add : public WorldPlugin
{
  public: void Load(physics::WorldPtr _parent, sdf::ElementPtr _sdf)
  {
    
    // Option 2: Insert model from string via function call.
    // Insert a sphere model from string
    sdf::SDF sphereSDF;
    sphereSDF.SetFromString(
       "<?xml version="1.0" ?>\
        <sdf version="1.5">\
          <world name="default">\

            <actor name="actor">\
              <skin>\
                <filename>moonwalk.dae</filename>\
                <scale>1.0</scale>\
              </skin>\
              <pose>0 0 0 0 0 0</pose>\
              <animation name="walking">\
                <filename>walk.dae</filename>\
                <scale>1</scale>\
                <interpolate_x>true</interpolate_x>\
              </animation>\
              </actor>\

            </world>\
          </sdf>");
        
                                      
    // Demonstrate using a custom model name.
    sdf::ElementPtr model = sphereSDF.Root()->GetElement("actor");
    model->GetAttribute("name")->SetFromString("actor");
    _parent->InsertModelSDF(sphereSDF);

  }
};

// Register this plugin with the simulator
GZ_REGISTER_WORLD_PLUGIN(Add)
}
